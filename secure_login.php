<?php

include_once('view.php');
require_once 'vendor/autoload.php';

include_once('models.php');
include('config.php');


function login(){

    include('config.php');
    $error = null;
    $inputs = array();
    $next = getitem($_REQUEST, 'next', '/');
    $username = getitem($_REQUEST, 'username');

    $llm = new LoginLogManager();
    $um = new UserManager();
    $pcm = new PasswordChallengeManager();

    if($username){


        $llm = new LoginLogManager();
        $vars = array();
        $vars['ip'] = $_SERVER['REMOTE_ADDR'];
        $vars['username'] = $username;
        $vars['success'] = 0;
        $noilsvo = $llm->number_of_invalid_logons_since_valid_one($username, 1);
        $last_invalid_logon = $llm->last_invalid_logon($username);
        $timeout = ($noilsvo * 10 + 30);

        if($noilsvo > 1 && $last_invalid_logon){
            $diff = time() - strtotime($last_invalid_logon->timestamp);
            $wait = $timeout - $diff;
            if($diff < $timeout){
                $error = "Incorrect password.";
                return render('secure_login.html', array('error' => $error, 'next' => $next, 'username' => $username, 'timeouterror' => True, 'wait' => $wait));
            }
        }
		$user = $um->get(array('username' => $username));

        if($user){   
            $vars['user_id'] = $user->user_id;
            $pcm = new PasswordChallengeManager();
            $password_challenges = $pcm->filter(array('user_id' => $user->user_id), 'used asc, id asc', 1);
            if(!$password_challenges){
                $error = "Ten user jest wygenerowany z bazy. Zaloguj się zwykłym formularzem wpisując \"{$username}\" jako hasło i użyj opcji zmień hasło";
                return render("error.html", array("error" => $error));
            } else {
                $password_challenge = $password_challenges[0];
            }
            $inputs = $password_challenge->get_inputs();
            $fields = $password_challenge->get_fields();

            $partial_password = '';
            foreach($fields as $field){
                $partial_password .= getitem($_REQUEST, "p{$field}");
            }
            if($partial_password){
                $system_salt = file_get_contents("salt");
                $password_hash = hash('sha256', $partial_password . $password_challenge->salt . $system_salt);
                if($password_hash == $password_challenge->hash){
                    $pcm->update(array('used' => $password_challenge->used + 1), array('id' => $password_challenge->id));
                    $vars['success'] = 1;
                    $_SESSION['user_id'] = $user->user_id;
                    header('Location: ' . $next);
                    $created = $llm->create($vars);
                    return;
                } else {
                    include("config.php");
                    $bledow = $noilsvo + 1;
                    echo "[TESTY] błędów: {$bledow}, dozwolone błędy: {$user->allowed_invalid_logons}";
                    $error = "Invalid password.";
                    $created = $llm->create($vars);
                    if($noilsvo >= $user->allowed_invalid_logons && $user->active){
                        // blokuj konto
                        $um = new UserManager();
                        $um->update(array('active' => 0), array('user_id' => $user->user_id));
                        $error .= " \nAccount disabled. Check your email for details.";
                        return render('secure_login.html', array('error' => $error, 'next' => $next, 'username' => $username));
                    } else if($noilsvo && $last_invalid_logon){
                        $wait = $timeout;
                        $timeouterror = 1;
                        return render('secure_login.html', array('error' => $error, 'next' => $next, 'username' => $username, 'timeouterror' => True, 'wait' => $wait));
                    }
                }
            }
        } else {

            include("config.php");
            
            srand(array_sum(array_map('ord', str_split($username))));
            $num_to_verify = rand(5, max(ceil($config['MAX_PASSWORD_LENGTH']/2), 5));

            // losuję pola do sprawdzenia
            $numbers = range(0, $config['MAX_PASSWORD_LENGTH'] - 1);
            shuffle($numbers);
            $fields = array_slice($numbers, 0, $num_to_verify);

            $inputs = array();
            for($i = 0; $i < $config['MAX_PASSWORD_LENGTH']; $i++){
                $inputs[$i] = new Input($i, !in_array($i, $fields));
            }
            $partial_password = '';
            foreach($fields as $field){
                $partial_password .= getitem($_REQUEST, "p{$field}");
            }
            if($partial_password){
                $allowed_invalid_logons = array_sum(array_map('ord', str_split($username))) % 6 + 2;
                echo "[TESTY] błędów: {$noilsvo}, dozwolone błędy: {$allowed_invalid_logons}";
                if($noilsvo == $allowed_invalid_logons){
                    $error .= " \nAccount disabled. Check your email for details.";
                    $created = $llm->create($vars);
                    return render('secure_login.html', array('error' => $error, 'next' => $next, 'username' => $username));
                } else if($noilsvo && $last_invalid_logon){
                    $wait = $timeout;
                    $error = "Incorrect password.";
                    $created = $llm->create($vars);
                    return render('secure_login.html', array('error' => $error, 'next' => $next, 'username' => $username, 'timeouterror' => True, 'wait' => $wait));
                } else {
                    $error = "Invalid password.";
                    $created = $llm->create($vars);
                }
            }
        }


    }
    return render('secure_login.html', array('error' => $error, 'next' => $next, 'username' => $username, 'inputs' => $inputs));

}

login();

?>
