<?php

session_start();

ini_set('error_reporting', E_ALL ^ E_DEPRECATED);
ini_set("display_errors", 1);

require_once 'vendor/autoload.php';
include_once('models.php');

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);


function getitem($array, $key, $default=null){
	if(isset($array[$key])){
		return $array[$key];
	}
	return $default;
}


function render($template, $context=array()){
	$context = array_merge(prepare_context(), $context);
	global $twig;
	echo $twig->render($template, $context);
}

function prepare_context(){
	$context = array();

	$um = new UserManager();
	$context['user'] = $um->get_logged_in();
	return $context;
}


function check_loggedin(){
	$um = new UserManager();
	$user = $um->get_logged_in();
	if($user->loggedin){
		return True;
	} else {
		$next= "$_SERVER[REQUEST_URI]";
		header('Location: /login.php?next=' . $next);
        return False;
	}
}

?>
