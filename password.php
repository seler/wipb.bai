<?php

include_once('view.php');
require_once 'vendor/autoload.php';
require_once('recaptchalib.php');

include_once('models.php');
include_once('config.php');

function generate_random_string($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+-=';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function rand32bit(){
    return rand(1073741824, 2147483647);
}



function password(){
    check_loggedin();
    $error = null;
    $password = getitem($_REQUEST, 'password');
    $password1 = getitem($_REQUEST, 'password1');
    $password2 = getitem($_REQUEST, 'password2');

    if($password && $password1 && $password2){
        include("config.php");

        $um = new UserManager();
        $llm = new LoginLogManager();
        $user = $um->get_logged_in();
        if(!$user->verify_password($password)){
            $error = "Invalid password.";
        } else if ($password1 != $password2){
            $error = "Passwords do not match.";
        } else if (!$password1 || !$password2){
            $error = "Password is empty.";
        } else if (strlen($password1)  < $config['MIN_PASSWORD_LENGTH']){
            $error = "Password is too short.";
        } else if (strlen($password1)  > $config['MAX_PASSWORD_LENGTH']){
            $error = "Password is too long.";
        } else {
            $ok = $user->set_password($password1);

            if($ok){
                session_destroy();
                return render('password_changed.html');
            } else {
                return render('error.html');
            }
        }
    }
    return render('password.html', array('error' => $error));

}

password();

?>
