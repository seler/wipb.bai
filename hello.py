#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = u"Rafal Selewonko <rselewonko@murator.com.pl>"

from flask import Flask
from flask import render_template
from flask import request
#from flask import make_response
from flask import abort, redirect, url_for
from flask import session, escape
import os

app = Flask(__name__)
app.config.from_object(__name__)


app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'flaskr.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))

app.secret_key = 'A0Zr98j/3yX R~XHH!jm0bvN]LWX/,?RT'

#app.config.from_envvar('FLASKR_SETTINGS', silent=True)


@app.route('/')
def index():
    if 'user_id' in session:
        return 'Logged in as %s' % escape(session['user_id'])
    return 'You are not logged in'


@app.route('/login/', methods=['GET', 'POST'])
def login():
    context = {}
    if request.method == 'POST':
        context['user_id'] = request.form.get('user_id')
        context['password'] = request.form.get('password')
        context['error'] = 'Invalid username/password'
        if context['user_id'] == context['password']:
            session['user_id'] = context['user_id']
            return redirect(url_for('index'))
        else:
            context['error'] = 'Invalid username/password'

    return render_template('login.html', **context)


@app.route('/logout/')
def logout():
    session.pop('user_id')
    return redirect(url_for('index'))


@app.route('/users/')
def users():
    return render_template('users.html', object_list=['a', 'b'])


@app.route('/messages/')
def messages():
    return "Lista wiadomosci"


@app.route('/messages/<int:id>/')
def display_message(id):
    return "Wiadomosc id %d" % id


@app.route('/messages/add/')
def add_message():
    return "Dodawanie tiadomosci"


@app.route('/messages/<int:id>/edit/')
def edit_message(id):
    abort(401)
    return redirect(url_for('login'))
    return "Edytowanie wiadomosci id %d" % id


@app.route('/messages/<int:id>/delete/')
def delete_message(id):
    return "Usuwanie wiadomosci id %d" % id


@app.route('/messages/<int:id>/allow/')
def allow_message(id):
    return u"Udostępnienie uprawnień do wiadomosci id %d" % id


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


if __name__ == "__main__":
    debug = True
    host = '0.0.0.0'
    port = 8001
    app.run(debug=debug, host=host, port=port)
