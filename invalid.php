<?php

include_once('view.php');
require_once 'vendor/autoload.php';
require_once('recaptchalib.php');

include_once('models.php');
include_once('config.php');


function invalid(){
    check_loggedin();
    $error = null;
    $um = new UserManager();
    $user = $um->get_logged_in();
    $new_noail = getitem($_REQUEST, 'noail');
    $noail = $user->allowed_invalid_logons;

    if($new_noail){
        $um->update(array('allowed_invalid_logons' => $new_noail), array('user_id' => $user->user_id));
        return render('invalid_ok.html');
    }
    return render('invalid.html', array('error' => $error, 'noail' => $noail));

}

invalid();

?>
