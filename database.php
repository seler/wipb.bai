<?php

include_once("class.MySQL.php");

class Database
{
	private static $instance;
	public $connection;
	private function __construct() {
		include("config.php");
		$this->connection = new MySQL(
			$config['DATABASE']['MYSQL_NAME'],
			$config['DATABASE']['MYSQL_USER'],
			$config['DATABASE']['MYSQL_PASS'],
		       	$config['DATABASE']['MYSQL_HOST']);
	}

	public static function getInstance ()
	{
		if (self::$instance === null) {
			self::$instance = new Database();
		}
		return self::$instance;
	}
}


?>
