<?php


class Model {

    public function __construct($row){
        foreach($row as $column => $value){
            $this->$column = $value;
        }
    }
}

class Manager {
    protected $db;
    public function __construct(){
        include_once('database.php');
        $this->db = Database::getInstance()->connection;
    }

    public function all($orderBy){
        $objects = array();
        foreach($this->db->Select($this->table, '', $orderBy) as $row){
            $objects[] = new $this->model($row);
        }
        return $objects;
    }
    public function get($where, $orderBy=''){
        $row = $this->db->Select($this->table, $where, $orderBy, 1)[0];
        if(is_array($row)){
            $object = new $this->model($row);
            return $object;
        } else {
            return null;
        }
    }

    public function delete($where){
        $row = $this->db->Delete($this->table, $where);
        return $row;
    }

    /*
    public function save($object){
        if(property_exists(get_class($object), $this->pk) && $object->$this->pk){
            // update
        } else {
            // create
        }
    }
     */

    public function create($vars){
        $ok = $this->db->Insert($vars, $this->table);
        if($ok){
            return $this->get(array($this->pk => $this->db->LastInsertId()));
        } else {
            return null;
        }
    }

    public function update($vars, $where){
        $row = $this->db->Update($this->table, $vars, $where);
        return $row;
    }

    public function filter($where, $orderBy='', $limit=''){
        $objects = array();
        $rows = $this->db->Select($this->table, $where, $orderBy, $limit);
        if(is_array($rows)) {
            foreach($rows as $row){
                $objects[] = new $this->model($row);
            }
        }
        return $objects;
    }
}


class AllowedMessage extends Model {
    public $message_id;
    public $user_id;
}

class AllowedMessagesManager extends Manager {
    protected $table = 'allowed_messages';
    protected $model = 'AllowedMessage';
}

class Message extends Model {
    public $message_id;
    public $text_;
    public $user_id;
    public $mod;
    public $allowed_user_ids = array();

    private $user = NULL;

    public function __construct($row){
        parent::__construct($row);
        $amm = new AllowedMessagesManager();
        $ams = $amm->filter(array('message_id' => $this->message_id));
        foreach($ams as $am){
            $this->allowed_user_ids[] = $am->user_id;
        }
    }

    public function get_user(){
        $um = new UserManager();
        if($this->user === NULL){
            $this->user = $um->get(array('user_id' => $this->user_id));
        }
        return $this->user;
    }

    public function user_allowed(){
        $um = new UserManager();
        $user = $um->get_logged_in();
        if($this->user_id == $user->user_id || in_array($user->user_id, $this->allowed_user_ids)){
            return True;
        }
        return False;
    }

    public function user_owner(){
        $um = new UserManager();
        $user = $um->get_logged_in();
        if($this->user_id == $user->user_id){
            return True;
        }
        return False;
    }

}

class MessageManager extends Manager {
    protected $table = 'messages';
    protected $model = 'Message';
    protected $pk = 'message_id';
}

class User extends Model {
    public $user_id = null;
    public $username = null;
    public $password_hash = null;
    public $salt = null;
    public $last_login = null;
    public $loggedin = true;
    public $number_of_invalid_logons_since_valid_one = null;

    public function __construct($row){
        parent::__construct($row);

        $llm = new LoginLogManager();
        $this->number_of_invalid_logons_since_valid_one = $llm->number_of_invalid_logons_since_valid_one($this->username);
        $this->last_valid_logon = $llm->last_valid_logon($this->username);
        $this->last_invalid_logon = $llm->last_invalid_logon($this->username);
    }

    public function verify_password($password){
        $system_salt = file_get_contents("salt");
        if($this->password_hash){
            return $this->password_hash == hash('sha256', $password . $this->salt . $system_salt);
        } else {
            return $this->username == $password;
        }
    }

    public function set_password($password){
        $number_of_challenges = 10;

        $pcm = new PasswordChallengeManager();
        $pcm->delete(array('user_id' => $this->user_id));
        for($i = 0; $i < $number_of_challenges; $i++){
            $pcm->make($this, $password);
        }

        $salt = generate_random_string(32);
        $system_salt = file_get_contents("salt");
        $password_hash = hash('sha256', $password . $salt . $system_salt);
        $vars = array(
            'salt' => $salt,
            'password_hash' => $password_hash,
        );
        $um = new UserManager();
        $um->update($vars, array('user_id' => $this->user_id));
        return true;
    }

}

class UserManager extends Manager {
    protected $table = 'users';
    protected $model = 'User';
    protected $pk = 'user_id';


    public function get_logged_in(){
        $user_id = getitem($_SESSION, 'user_id', null);
        $row = $this->db->Select($this->table, array('user_id' => $user_id), '', 1)[0];
        if(is_array($row)){
            $object = new $this->model($row);
        } else {
            $object = new $this->model(array('loggedin' => false));
        }
        return $object;
    }
}

class LoginLog extends Model {

}

class LoginLogManager extends Manager {
    protected $table = 'login_logs';
    protected $model = 'LoginLog';
    protected $pk = 'log_id';


    public function last_invalid_logon($username){
        $logs = $this->filter(array('username' => $username, 'success' => 0), 'timestamp desc', 1);
        if ($logs){
            return $logs[0];
        } else {
            return 0;
        }
    }

    public function last_valid_logon($username, $cheat=2){
        $logs = $this->filter(array('username' => $username, 'success' => 1), 'timestamp desc', $cheat);
        if ($logs && count($logs) == $cheat){
            return $logs[$cheat - 1];
        } else {
            return 0;
        }
    }

    public function last_logon($username){
        $logs = $this->filter(array('username' => $username), 'timestamp desc', 1);
        if ($logs && count($logs) == 1){
            return $logs[0];
        } else {
            return null;
        }
    }

    public function number_of_invalid_logons_since_valid_one($username, $cheat=2){

        $query = "SELECT * FROM `{$this->table}` WHERE ";

        $lvl = $this->last_valid_logon($username, $cheat);
        if($lvl){
            $query .= "`timestamp` > '{$lvl->timestamp}' AND ";
        }
        $query .= "`success` = '0' AND `username` = '{$username}' ";

        $query .= 'ORDER BY `timestamp` DESC';

        $rows =  $this->db->ExecuteSQL($query);
        if(is_array($rows)){
            return count($rows);
        } else {
            return 0;
        }

    }
}

class Input {
    public $index = null;
    public $disabled = null;

    function __construct($index, $disabled){
        $this->index = $index;
        $this->disabled = $disabled;
    }
}


class PasswordChallenge extends Model {
/*
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT REFERENCES users(user_id),
    fields VARCHAR(255) NULL,
    hash VARCHAR(255) NULL,
    salt VARCHAR(255) NULL,
    used INT NULL
 */
    public function get_fields(){
        return explode(',', $this->fields);
    }
    public function get_inputs(){
        include("config.php");
        $inputs = array();
        $fields = $this->get_fields();
        for($i = 0; $i < $config['MAX_PASSWORD_LENGTH']; $i++){
            $inputs[$i] = new Input($i, !in_array($i, $fields));
        }
        return $inputs;
    }
}

class PasswordChallengeManager extends Manager {
    protected $table = 'password_challenges';
    protected $model = 'PasswordChallenge';
    protected $pk = 'id';

    public function make($user, $password){
        srand();
        $password_array = str_split($password);
        $password_length = strlen($password);
        $num_to_verify = max(ceil($password_length/2), 5);

        // losuję pola do sprawdzenia
        $numbers = range(0, $password_length - 1);
        shuffle($numbers);
        $fields = array_slice($numbers, 0, $num_to_verify);

        $salt = generate_random_string(32);
        $system_salt = file_get_contents("salt");
        $partial_password = '';
        foreach($fields as $field){
            $partial_password .= $password_array[$field];
        }
        $password_hash = hash('sha256', $partial_password . $salt . $system_salt);
        $vars = array(
            'user_id' => $user->user_id,
            'fields' => implode(',', $fields),
            'hash' => $password_hash,
            'salt' => $salt
        );
        $created = $this->create($vars, array('user_id' => $user->user_id));
    }

}


?>
