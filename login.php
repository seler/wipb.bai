<?php

include_once('view.php');
require_once 'vendor/autoload.php';

include_once('models.php');
include_once('config.php');


function login(){
	$error = null;
	$next = getitem($_REQUEST, 'next');
    $timeouterror = 0;
    $wait = 0;

	if(isset($_REQUEST['username']) && isset($_REQUEST['password'])){
		$error = "Incorrect username or password.";
		$username = $_REQUEST['username'];
		$password = $_REQUEST['password'];
		$um = new UserManager();
		$llm = new LoginLogManager();
		$user = $um->get(array('username' => $username));
		$vars = array();
		$vars['ip'] = $_SERVER['REMOTE_ADDR'];
		$vars['username'] = $username;
		$vars['success'] = 0;

		if($user){
			$noilsvo = $llm->number_of_invalid_logons_since_valid_one($username, 1);
			$last_invalid_logon = $llm->last_invalid_logon($username);
			$vars['user_id'] = $user->user_id;
            $timeout = ($noilsvo * 10 + 30);
			if($noilsvo > 1 && $last_invalid_logon){
			    // pierwsza nieudana proba gratis
			    
			    // ilosc czasu od ostatniej nieudanej proby
			    $diff = time() - strtotime($last_invalid_logon->timestamp);
			    
			    // ile jeszcze trzeba poczekac
    	        $wait = $timeout - $diff;
    	        
			    if($diff < $timeout){
                    $error = "Incorrect username or password.";
                	return render('login.html', array('error' => $error, 'timeouterror' => True, 'next' => $next, 'wait' => $wait));
			    }
			}

			if($user->verify_password($password) && $user->active){
				$vars['success'] = 1;
				$_SESSION['user_id'] = $user->user_id;
				$next = getitem($_REQUEST, 'next', '/');
				header('Location: ' . $next);
			} else {
				include("config.php");
				if($noilsvo >= $user->allowed_invalid_logons && $user->active){
					$um = new UserManager();
					$um->update(array('active' => 0), array('user_id' => $user->user_id));
					$error .= " \nAccount disabled. Check your email for details.";
				} else {
                    if($noilsvo && $last_invalid_logon){
                        $wait = $timeout;
                        $timeouterror = 1;
                    }
                }
			}
		} else {
            // nie ma usera, ale fejkujemy wszystko
			$noilsvo = $llm->number_of_invalid_logons_since_valid_one($username, 1);
			$last_invalid_logon = $llm->last_invalid_logon($username);
            $timeout = ($noilsvo * 10 + 30);
            include("config.php");
            $allowed_invalid_logons = array_sum(array_map('ord', str_split($username))) % 6 + 2;
            if($noilsvo == $allowed_invalid_logons){
                $error .= " \nAccount disabled. Check your email for details.";
            } else if($noilsvo && $last_invalid_logon){
			    // pierwsza nieudana proba gratis
			    
			    // ilosc czasu od ostatniej nieudanej proby
			    $diff = time() - strtotime($last_invalid_logon->timestamp);
			    
			    // ile jeszcze trzeba poczekac
    	        $wait = $timeout - $diff;
    	        
			    if($diff < $timeout){
                    $error = "Incorrect username or password.";
                	return render('login.html', array('error' => $error, 'timeouterror' => True, 'next' => $next, 'wait' => $wait));
			    } else {
                    $wait = $timeout;
                    $timeouterror = 1;
                }
			}
        }
		$created = $llm->create($vars);
	}
    return render('login.html', array('error' => $error, 'timeouterror' => $timeouterror, 'next' => $next, 'wait' => $wait));

}

login();

?>
