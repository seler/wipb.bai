<?php

include_once('view.php');
require_once 'vendor/autoload.php';
require_once('recaptchalib.php');

include_once('models.php');
include_once('config.php');

function generate_random_string($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+-=';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}



function register(){
	$error = null;
	$captcha_error = null;
	$next = getitem($_REQUEST, 'next');
	$username = getitem($_REQUEST, 'username');
	$email = getitem($_REQUEST, 'email');
	$password1 = getitem($_REQUEST, 'password1');
	$password2 = getitem($_REQUEST, 'password2');
    $publickey = "6LeFnPASAAAAALHjHLyqY8VrHH-O1DxEYzMAz8lf";
    $privatekey = "6LeFnPASAAAAAHRRDO9kXSubwhQKzKM1ucN1A8_t";
    $recaptcha_response_field = getitem($_REQUEST, "recaptcha_response_field");
	
	if($recaptcha_response_field && $username && $email && $password1 && $password2){
	    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_REQUEST["recaptcha_challenge_field"], $_REQUEST["recaptcha_response_field"]);
	    if($resp->is_valid){
		    $um = new UserManager();
		    $user_username = $um->get(array('username' => $username));
		    $user_email = $um->get(array('email' => $email));
            include("config.php");
		    if($user_username){
		        $error = "Username \"{$username}\" already registered.";
		    } else if ($user_email){
		        $error = "Email \"{$email}\" already registered.";
            } else if ($password1 != $password2){
                $error = "Passwords do not match.";
            } else if (!$password1 || !$password2){
                $error = "Password is empty.";
            } else if (strlen($password1)  < $config['MIN_PASSWORD_LENGTH']){
                $error = "Password is too short.";
            } else if (strlen($password1)  > $config['MAX_PASSWORD_LENGTH']){
                $error = "Password is too long.";
            } else {
		        $vars = array(
		            'username' => $username,
		            'email' => $email,
		            'active' => 1, // tymczasowo, docelowo nieaktywne i trzeba aktytować klikając w mail
		        );
		        $user = $um->create($vars);
                $ok = $user->set_password($password1);
		        if($user && $ok){
                    return render('registered.html');
		        } else {
                    return render('error.html');
		        }
		    }
		} else {
		    $captcha_error = $resp->error;
		    $error = "Incorrect captcha solution.";
		}
	} else {
	    //$error = "Please fill in all fields.";
	    // nic nie wyswietlam, sprawdzanie jest po stronie html, a jak ktos bez html to wysyla to bez info
	}
    $captcha = recaptcha_get_html($publickey, $captcha_error);
    return render('register.html', array('error' => $error, 'next' => $next, 'captcha' => $captcha));

}

register();

?>
