<?php

ini_set('error_reporting', E_ALL ^ E_DEPRECATED);
ini_set("display_errors", 1);

include_once('view.php');
include_once('models.php');


function check_user_allowed($message){
    if($message->user_allowed()){
        return true;
    } else {
        render("error.html", array('error' => 'Not allowed'));
        return false;
    }
}


function check_user_owner($message){
    if($message->user_owner()){
        return true;
    } else {
        render("error.html", array('error' => 'Not allowed'));
        return false;
    }
}


function list_messages(){
    $mm = new MessageManager();
    $messages = $mm->all('`mod` desc');

    return render('messages.html', array('messages' => $messages));
}

function add_message(){
    $loggedin = check_loggedin();

    $text = getitem($_REQUEST, 'text');
    if($loggedin && $text){
        $um = new UserManager();
        $mm = new MessageManager();
        $user = $um->get_logged_in();
        $created = $mm->create(array('text' => $text, 'user_id' => $user->user_id, 'mod' => date('Y-m-d %H:%i:%s')));
        $next = getitem($_REQUEST, 'next', '/messages.php');
        header('Location: ' . $next);
    }

    return render('message_form.html', array('action' => 'add'));
}

function edit_message(){
    $loggedin = check_loggedin();

    $message_id = getitem($_REQUEST, 'message_id');
    $mm = new MessageManager();
    $message = $mm->get(array('message_id' => $message_id));
    if($message == null){
        return render('error.html', array('error' => 'Message not found'));
    }
    $text = getitem($_REQUEST, 'text');
    $allowed = check_user_allowed($message);
    if($loggedin && $allowed){
        if($text){
            $created = $mm->update(array('text' => $text, 'mod' => date('Y-m-d %H:%i:%s')), array('message_id' => $message_id));
            $next = getitem($_REQUEST, 'next', '/messages.php');
            header('Location: ' . $next);
        }

        return render('message_form.html', array('action' => 'edit', 'message' => $message));
    }
}

function grant_access(){
    if(check_loggedin()){

        $context = array();
        $message_id = getitem($_REQUEST, 'message_id');
        $context['message_id'] = $message_id;
        $mm = new MessageManager();
        $message = $mm->get(array('message_id' => $message_id));

        if($message == null){
            return render('error.html', array('error' => 'Message not found'));
        }
        $allowed = check_user_owner($message);
        if($allowed){

            if($message_id){
                $username = getitem($_REQUEST, 'username');
                if($username){
                    $um = new UserManager();
                    $user = $um->get(array('username' => $username));
                    $amm = new AllowedMessagesManager();
                    $created = $amm->create(array('message_id' => $message_id, 'user_id' => $user->user_id));
                    if($created){
                        return render('message_access_granted.html', $context);
                    }

                } else {
                    $context['message_id'] = $message_id;
                    return render('message_access_form.html', $context);
                }
            }
            return render('error.html', $context);
        }
    }
}

function revoke_access(){
    if(check_loggedin()){

        $context = array();
        $message_id = getitem($_REQUEST, 'message_id');
        $context['message_id'] = $message_id;
        $mm = new MessageManager();
        $message = $mm->get(array('message_id' => $message_id));

        if($message == null){
            return render('error.html', array('error' => 'Message not found'));
        }
        $allowed = check_user_owner($message);
        if($allowed){
            if($message_id){
                $user_id= getitem($_REQUEST, 'user_id');
                if($user_id){
                    $amm = new AllowedMessagesManager();
                    $created = $amm->delete(array('message_id' => $message_id, 'user_id' => $user_id));
                    if($created){
                        return render('message_access_granted.html', $context);
                    }

                } else {
                    $context['message_id'] = $message_id;
                    $amm = new AllowedMessagesManager();
                    $um = new UserManager();
                    $allowed_messages = $amm->filter(array('message_id' => $message_id));
                    $context['allowed_users'] = array();
                    foreach($allowed_messages as $am){
                        $context['allowed_users'][] = $um->get(array('user_id' => $am->user_id));
                    }
                    return render('message_revoke_form.html', $context);
                }
            }
            return render('error.html', $context);
        }
    }
}


function delete_message(){
    if(check_loggedin()){

    $message_id = getitem($_REQUEST, 'message_id');
    $mm = new MessageManager();
    $message = $mm->get(array('message_id' => $message_id));
    if($message == null){
        return render('error.html', array('error' => 'Message not found'));
    }
    $allowed = check_user_owner($message);
    if($allowed){

        $context = array('message' => $message);
        if($message_id){
            $confirm = getitem($_REQUEST, 'confirm');
            if($confirm){
                $deleted = $mm->delete(array('message_id' => $message_id));
                if($deleted){
                    $next = getitem($_REQUEST, 'next', '/messages.php');
                    header('Location: ' . $next);
                    return;
                }
            } else {
                return render('message_delete.html', $context);
            }
        }
        return render('error.html', $context);
    }
    }
}

if(isset($_REQUEST['action'])){
    $action = $_REQUEST['action'];
} else {
    $action = 'list';
}

if($action == 'add'){
    add_message();
} else if($action == 'edit'){
    edit_message();
} else if($action == 'grant_access'){
    grant_access();
} else if($action == 'revoke_access'){
    revoke_access();
} else if($action == 'delete'){
    delete_message();
} else {
    list_messages();
}

?>
